set(MI_SOURCES
  vtkComputeMoments.cxx
  vtkmComputeMoments.cxx
  vtkMomentInvariants.cxx
  vtkMomentsHelper.cxx
  vtkReconstructFromMoments.cxx
  vtkSimilarityBalls.cxx
)

set(MI_HEADERS
  vtkComputeMoments.h
  vtkMomentInvariants.h
  vtkMomentsHelper.h
  vtkReconstructFromMoments.h
  vtkSimilarityBalls.h
)

if (TARGET vtkm::cuda)
  set_source_files_properties(
    vtkmComputeMoments.cxx
    PROPERTIES LANGUAGE "CUDA")
endif ()

vtk_module_add_module(VTK::MomentInvariants
  SOURCES ${MI_SOURCES}
  HEADERS ${MI_HEADERS})
